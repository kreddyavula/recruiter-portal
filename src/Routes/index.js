import React, { Component } from 'react';

import {
    BrowserRouter as Router,
    // Link.
    Route,
    Switch
} from 'react-router-dom';

import SigninComponent  from '../Components/Signin';
import  SignupComponent  from '../Components/Signup';
import { createBrowserHistory } from "history";
import  InstantJobsComponent  from "../Components/Instant_Jobs";
import HeaderComponent from "../Components/Header/index";
import ProfileComponent from "../Components/Profile";

const history = createBrowserHistory();

class AppRoutes extends Component {
    constructor(props) {
        super(props);
    }

    render() {
       

        console.log('App Render called')

        return (

            <Router history={history}>
                <Switch>
                <Route exact path="/" component={SigninComponent} />
                <Route exact path="/login" component={SigninComponent} />
                <Route exact path="/signup" component={SignupComponent} />
                <Route exact path="/instantjobs" component={InstantJobsComponent} />
                <Route exact path="/header" component={HeaderComponent} />
                <Route exact path="/profile" component={ProfileComponent} />
                
                </Switch>
            </Router>
            // <div>
            //     Welcome
            // </div>

        );

    }
}

export default AppRoutes;


