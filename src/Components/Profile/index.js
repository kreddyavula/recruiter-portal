

import React, { Component } from 'react';

// import './signin.scss'
import '../Profile/profile.scss';
import HeaderComponent from '../Header/index';
import SidemenuComponent from '../Sidemenu/index';
import axios from 'axios';
import { APP_URLS } from '../../Common/Url';
// import { ToastContainer, toast } from 'react-toastify';
import { ToastsContainer, ToastsContainerPosition, ToastsStore } from 'react-toasts';

import Service from '../../Common/service';

var cms = new Service();

export default class ProfileComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {

            token: "",

            selectzone: '',



            RoleNames: [],
            Roles: {
                PARENTID: 1
            },
            showLoading: false,

            countries: [],
            employtypedropdown: [],

            empoyeetype: "",
            country: "",
            name: "",
            gender: "",
            taxidentitynum: "",
            age: '',
            IsActive:1,
            ProfileId: "",

        }

        // this.navToSignup()

        this.retriveData();


    }

    retriveData = async () => {
        const value = await localStorage.getItem('token');
        if (value !== null) {
            this.setState({ token: value });
            this.getCountryList();
            this.getDropdownsMethod();
            this.getUserProfile();
        }

    }

    getCountryList = () => {
        let self = this;
        let url = APP_URLS.getCountryDropDown;

        cms.commonGetCall(url, function (res) {

            console.log("kjflkasflk", res);

            if (res.status == 200) {
                self.setState({ countries: res.data })
            }

            // this.method2()

        })



        /// call back example

        // example = () => {

        //     method1 = (data, function (res) {

        //         method2(res)

        //     })
        // }


        // method1 = (data, callback) => {
        //     let name = "reddy"
        //     callback(name)
        // }


        // method2 = (res) => {
        //     jfsalfhlasjflkasj
        // }


        // this.getCall(url).then(resp => {
        //     console.log("countries", resp);

        //     if (resp.status == 200) {
        //         this.setState({ countries: resp.data })
        //     }

        // })

    }



    getDropdownsMethod = () => {
        // let url = APP_URLS.categories;
        // let payload = {
        //     categories: [1, 5, 22, 50, 75, 115]
        // }

        this.postCall2().then(res => {
            console.log("dropdown data", res);
            if (res.status == 200) {
                this.setState({ employtypedropdown: res.data.EmployementType })
            }

        })

    }


    getUserProfile = () => {
        let url = APP_URLS.Getprofile;
        this.getCall(url).then(res => {
            console.log("profile data", res);
            if (res.status == 200) {
                this.setState({ name: res.data[0].CandidateName })
                if (res.data[0].Age) {
                    this.setState({ age: res.data[0].Age.toString() })
                }
                this.setState({ ProfileId: res.data[0].ProfileId })
                this.setState({ gender: res.data[0].Gender })
                this.setState({ taxidentitynum: res.data[0].TaxIdentityNumber })
                this.setState({ country: res.data[0].CountryId })
                this.setState({ empoyeetype: res.data[0].EmploymentTypeId })
                this.setState({ IsActive: res.data[0].IsActive });
            }
        })

    }



    getCall = (url) => {
        return axios.get(url,
            {
                headers: {
                    Authorization: this.state.token
                }
            }
        ).then((res) => {
            return res.data;
        })
            .catch(function (e) {
                console.log('ERROR ', e);
            })

    }

    postCall2 = () => {


        return axios.post(APP_URLS.categories,
            {
                categories: [1, 5, 22, 50, 75, 115]

            },
            {
                headers: {
                    Authorization: this.state.token
                }
            }
        ).then((result) => {
            console.log("check ", result.data);
            return result.data;

        })
            .catch(function (e) {
                console.log('ERROR ', e);
            })
    }



    handleChange = () => {

    }

    handleSubmit = () => {

    }


    signIn = () => {
        // console.log("zone", this.state.selectzone);
        // console.log("email",this.state.Email);
        // console.log("password",this.state.Password);

        this.postCall().then(res => {
            console.log("hsdkjsdhjs", res);

            if (res && res.status == 200) {
                ToastsStore.success(res.message)
                localStorage.setItem('ProfileName', "Sai");
                localStorage.setItem('token', res.data.token);
                this.props.history.push('/instantjobs');
            }
            if (res && res.status == 400) {
                ToastsStore.warning(res.message)
            }
            else {
                ToastsStore.error("Please Try again")
            }

        })

    }


    postCall = () => {

        let urlis = APP_URLS.LOGIN + '/' + this.state.selectzone;

        let payload = {
            'Email': this.state.Email,
            'Password': this.state.Password
        }


        return axios.post(urlis,
            payload,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then((res) => {
                console.log("login responce", res);
                return res.data;

            })
            .catch(function (e) {
                console.log('ERROR ', e);
            })
    }




    updateProfile = () => {

        let url = APP_URLS.updateprofile;

        let payload = {
            CandidateName: this.state.name,
            Functions: 3,
            CurrentCTC: "",
            ExpectedCTC: "",
            Skills: "java",
            MobileNumber: "9898898989",
            Location: "hyd",
            EmploymentTypeId: this.state.empoyeetype,
            ExpertListing: "",
            FreelancerListing: "",
            ProfileOpen: this.state.IS_PROFILE_OPEN,
            ProfileLocked: this.state.IS_PROFILE_LOCK,
            ProfilePicture: "",
            TaxIdentity: this.state.taxidentitynum,
            TextResumeUrl: "",
            VideoResumeUrl: "",
            Age: this.state.age,
            TaxIdentityNumber: this.state.taxidentitynum,
            CountryId: this.state.country,
            CanndidateSkills: "",
            Gender: this.state.gender,
            ProfileId: this.state.ProfileId,
            IsActive: this.state.IsActive
        }
        let self = this;
        cms.commonPostMethod(url, payload, function (res) {

            console.log("kjflkasflk", res);

            if (res.status == 200) {
                console.log("the profile upadate", res);
                self.getUserProfile()
            }

        })

    }

    _isActive = (val) => {
        this.setState({ IsActive: val });
        
    }



    render() {

        var countriesdropdown = this.state.countries.map(item => {
            return <option value={item.CountryId}>{item.CountryName}</option>
        })

        var selectemplyooestypelist = this.state.employtypedropdown.map(item => {
            return <option value={item.LID}>{item.Name}</option>
        })

        return (

            <div>

                <HeaderComponent />
                <div className="row">
                    <div className="col-md-4">
                        <SidemenuComponent />
                    </div>
                    <div className="col-md-4">

                        <ToastsContainer position={ToastsContainerPosition.TOP_RIGHT} store={ToastsStore} />


                        <div className="sign-in">Profile</div>
                        <div className="my-stytle">


                            <select id="dropdown" value={this.state.country}
                                onChange={(e) => this.setState({ country: e.target.value })}>

                                <option value="">Select country</option>
                                {countriesdropdown}


                            </select>
                            <br></br>




                            <input type="text" className="input-styles" autoComplete="off"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter Name"

                                name='name'
                                value={this.state.name}
                                onChange={(e) => this.setState({ name: e.target.value })}

                            />


                            <input type="text" autoComplete="off" className="input-styles"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter tax identity number"

                                name='taxidentitynum'
                                value={this.state.taxidentitynum}
                                onChange={(e) => this.setState({ taxidentitynum: e.target.value })}

                            />
                            <br></br>

                            <select id="dropdown" value={this.state.empoyeetype}
                                onChange={(e) => this.setState({ empoyeetype: e.target.value })}>

                                <option value="">Select employe type</option>
                                {selectemplyooestypelist}


                            </select>
                            <br></br>
                            <br></br>


                            <select id="dropdown" value={this.state.gender}
                                onChange={(e) => this.setState({ gender: e.target.value })}>

                                <option value="">Gender</option>
                                <option value="1">male</option>
                                <option value="2">Female</option>
                                <option value="3">Transgender</option>

                            </select>

                            <br></br>
                            <input type="number" autoComplete="off" className="input-styles"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="age"
                                max="2"
                                name='age'
                                value={this.state.age}
                                onChange={(e) => this.setState({ age: e.target.value })}


                            />

                            {this.state.IsActive == 0 ?
                                
                                <div onClick= {() => this._isActive(1)} >

                                    <div className="activebtn"> Active your Profile</div>
                                </div>
                            : null}


                            {this.state.IsActive == 1 ? 
                                
                                <div onClick= {() => this._isActive(0)}>

                                    <div className="inactivebtn"> Inactive your Profile </div>
                                </div>
                            : null}




                            <div>
                                <button type="button" className="signup-btn"
                                    onClick={this.updateProfile}
                                >update</button>
                            </div>

                        </div>

                    </div>

                    <div className="col-md-4">
                    </div>


                </div>


            </div>

        )

    }
}


