

import React, { Component } from 'react';

// import './signin.scss'
import '../Signin/signin.scss';
import HeaderComponent from '../Header/index';
import SidemenuComponent from '../Sidemenu/index';
import axios from 'axios';
import { APP_URLS } from '../../Common/Url';
// import { ToastContainer, toast } from 'react-toastify';
import { ToastsContainer, ToastsContainerPosition, ToastsStore } from 'react-toasts';

export default class SigninComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {

            activeis: false,

            selectzone: '',



            RoleNames: [],
            Roles: {
                PARENTID: 1
            },
            showLoading: false,
            Email: "",
            Password: "",
            LOGGED_IN_ROLE: "",
            formErrors: {
                Email: "",

                Password: "",
                LOGGED_IN_ROLE: "",
            }
        }

        // this.navToSignup()

        this.retriveData()

    }

    retriveData = async () => {
        const value = await localStorage.getItem('token');
        if (value !== null) {
            // this.props.history.push('/instantjobs');
        }

    }



    handleChange = () => {

    }

    handleSubmit = () => {

    }


    signIn = () => {
        // console.log("zone", this.state.selectzone);
        // console.log("email",this.state.Email);
        // console.log("password",this.state.Password);

        this.postCall().then(res => {
            console.log("hsdkjsdhjs", res);

            if (res && res.status == 200) {
                ToastsStore.success(res.message)
                localStorage.setItem('ProfileName', "Sai");
                localStorage.setItem('token', res.data.token);
                this.props.history.push('/profile');
            }
            if (res && res.status == 400) {
                ToastsStore.warning(res.message)
            }
            else {
                ToastsStore.error("Please Try again")
            }

        })

    }


    postCall = () => {

        let urlis = APP_URLS.LOGIN + '/' + this.state.selectzone;

        let payload = {
            'Email': this.state.Email,
            'Password': this.state.Password
        }


        return axios.post(urlis,
            payload,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then((res) => {
                console.log("login responce", res);
                return res.data;

            })
            .catch(function (e) {
                console.log('ERROR ', e);
            })
    }




    navToSignup = () => {
        this.props.history.push('/signup');
    }

    active = () => {
        this.setState({ activeis: true })
    }

    deactive = () => {
        this.setState({ activeis: false })
    }


    render() {

        return (

            <div>
                {/* <HeaderComponent/> */}



                <div className="row">
                    <div className="col-md-4">
                        {/* <SidemenuComponent/> */}
                    </div>
                    <div className="col-md-4">

                        <ToastsContainer position={ToastsContainerPosition.TOP_RIGHT} store={ToastsStore} />

                        <div className="sign-in">WELCOME</div>
                        <div className="sign-in">Signin</div>
                        <div className="my-stytle">

                            {/* dropdown */}

                            <select id="dropdown" value={this.state.selectzone}
                                onChange={(e) => this.setState({ selectzone: e.target.value })}>

                                <option value="">Please Select Zone</option>
                                <option value="2">candidate</option>
                                <option value="3">employee</option>

                            </select>
                            <br></br>




                            <input type="test" className="input-styles" autoComplete="off"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter email"

                                name='Email'
                                value={this.state.Email}
                                onChange={(e) => this.setState({ Email: e.target.value })}
                            // onChange={this.handleChange}
                            // onKeyPress={this.handleKeyPress}

                            />


                            <input type="password" autoComplete="off" className="input-styles"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter Password"

                                name='Password'
                                value={this.state.Password}
                                onChange={(e) => this.setState({ Password: e.target.value })}
                            // onKeyPress={this.handleKeyPress}

                            />
                            <br></br>

                            {/* {this.state.activeis ?
                                <div>Profile in active</div>
                                : null
                            }
                            {!this.state.activeis ?
                                <div>Profile in deactive</div>
                                : null} */}





                            <div>
                                <button type="button" className="signin-btn"
                                    onClick={this.signIn}
                                >SIGN- IN</button>
                            </div>

                            <div>
                                <button type="button" className="signup-btn"
                                    onClick={this.navToSignup}
                                >SIGN-Up</button>
                            </div>


                            {/* <div>
                                <button type="button" className="signup-btn"
                                    onClick={this.active}
                                >active</button>
                            </div> 
                             <div>
                                <button type="button" className="signup-btn"
                                    onClick={this.deactive}
                                >deactive</button>
                            </div>  */}

                        </div>

                    </div>
                    <div className="col-md-4">

                    </div>

                </div>


            </div>

        )

    }
}

// function mapStateToProps(state, actions) {
//     return state;
// }
// export default connect(mapStateToProps)(withRouter(SignInComponent));
