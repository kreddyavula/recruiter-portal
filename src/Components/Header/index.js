

import React, { Component } from 'react';
import "../Header/header.scss"

export default class HeaderComponent extends Component {
    constructor() {
        super();
        this.state = {
        }

    }

    logout = () => {
        localStorage.clear();
        // this.props.history.push('/login');
    }


    render() {


        return (
            <div>
                <div className="bckcolor">
    <div className="row">
        <div className="col-md-2">
            <h2>LOGO</h2>
        </div>
        <div className="col-md-10">
            <nav className="navbar navbar-expand-sm justify-content-end">
                <ul className="navbar-nav">
                    {/* <li className="nav-item">
                        <a className="nav-link" href="javascript:void(0)" >Sign In</a>
                    </li> */}
                    <li className="nav-item">
                        <button className="nav-link" onClick={this.logout} >Log Out</button>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

            </div>
        )

    }
}
