import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppRoutes from './Routes';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (
    <div>
    {/* <Router> */}
    <AppRoutes />
{/* </Router> */}
</div>
  );
}

export default App;
